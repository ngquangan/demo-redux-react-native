import { createActions, createReducer } from 'reduxsauce';
import {Map} from 'immutable';

export const { Types, Creators } = createActions({ //tu tao type theo ten reducer
    addProduct: null, // khong co tham so
    deleteProduct: ["id"],
    fetchProducts: null,
    fetchProductsSuccessed: null,
    fetchProductsFailed: null
});

const INITIAL_STATE = Map({
    type: '',
    products: null,
    error: null
});

console.log(INITIAL_STATE);

const addProduct = (state, action) => {
    
    return {
        ...state,
        type: action.type
    }
}

const fetchProducts = (state = INITIAL_STATE, action) => {
    console.log(state);
    //chổ ni sao state k là immutable
    return {
        ...state,
        type: action.type
    }
}

const fetchProductsSuccessed = (state, action) => {
    return state.merge({

        type: action.type,
        products: action.products
    });
}

const fetchProductsFailed = (state, action) => {
    return state.merge({
        type: action.type,
        error: action.error
    });
}

const HANDLERS = {
    [Types.ADD_PRODUCT]: addProduct,
    [Types.FETCH_PRODUCTS]: fetchProducts,
    [Types.FETCH_PRODUCTS_SUCCESSED]: fetchProductsSuccessed,
    [Types.FETCH_PRODUCTS_FAILED]: fetchProductsFailed
}

const productReducer = createReducer(INITIAL_STATE, HANDLERS)

export {productReducer}

