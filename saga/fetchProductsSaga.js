import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import {create} from 'apisauce';

const api = create({
    baseURL: 'http://localhost:3000',
  })

function* fetchProducts() {
    try{
        const result = yield call(() => api.get('/products'));
        if(result.status === 200) {
            yield put({
                type: "FETCH_PRODUCTS_SUCCESSED",
                products: result.data
            })
        }
    }catch(err){
        yield put({
            type: "FETCH_PRODUCTS_FAILED",
            error: err
        })
    }
}

function* fetchProductsSaga() {
    yield takeLatest("FETCH_PRODUCTS", fetchProducts);
  }

  export default fetchProductsSaga;