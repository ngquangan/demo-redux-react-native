/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import HomeScreen from '@home/components/HomeScreen';
import InfoScreen from '@info/components/InfoScreen';
import InfoScreenContainer from '@info/container/InfoScreenContainer';

import {createStackNavigator} from 'react-navigation';


import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux';
import {
  Provider,
  connect
} from 'react-redux';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';

import {createLogger} from 'redux-logger';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react'
import immutableTransform from 'redux-persist-transform-immutable';

import createSagaMiddleware from 'redux-saga'
import fetchProductsSaga from '@saga/fetchProductsSaga'
const sagaMiddleware = createSagaMiddleware();

import {productReducer} from '@reducers/productReducer';

const persistConfig = {
  transform: [immutableTransform()],
  key: 'root',
  storage,
}


let routerConfig = {
  Home: {
    screen: HomeScreen
  },
  Info: {
    screen: InfoScreenContainer
  }
}

let stackNavigatorConfig = {

}

let AppNavigator = createStackNavigator(routerConfig, stackNavigatorConfig)

let navReducer = createNavigationReducer(AppNavigator);

let appReducer = combineReducers({
  nav: navReducer,
  products: productReducer
})

const persistedReducer = persistReducer(persistConfig, appReducer);

let middleware = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav
)

let mainApp = reduxifyNavigator(AppNavigator, "root");

const mapStateToProps = (state) => ({
  state: state.nav
})

const AppWithNavigationState = connect(mapStateToProps)(mainApp);

const store = createStore(
  persistedReducer,
  applyMiddleware(middleware,createLogger(), sagaMiddleware),
);

sagaMiddleware.run(fetchProductsSaga);

let persistor = persistStore(store);

export default class App extends Component {
  render() {
    return (
        <Provider
          store = {store}
        >
          <PersistGate
            loading = {null}
            persistor = {persistor}
          >
            <AppWithNavigationState />
          </PersistGate>
        </Provider>
    );
  }
}

