import {Creators} from '@reducers/productReducer';

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

import InfoScreen from '@info/components/InfoScreen';

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
}
const mapDispatchToProps =  dispatch => bindActionCreators(Creators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InfoScreen);