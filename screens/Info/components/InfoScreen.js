import React, { Component } from 'react'
import {View, Text,SafeAreaView,TouchableOpacity, ActivityIndicator} from 'react-native';
export default class InfoScreen extends Component {
  render() {
    return (
        <View
            style = {
                {
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }
            }
        >
            <Text>This is InfoScreen</Text>
            <TouchableOpacity
                onPress = {
                    () => this.props.navigation.goBack()
                }
            >
                <View
                    style = {
                        {
                            padding: 10,
                            margin: 30,
                            backgroundColor: 'purple'
                        }
                    }
                >
                    <Text
                        style = {
                            {
                                color: '#fff',
                                fontSize: 20
                            }
                        }
                    >Goback to Home Screen</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity
                onPress = {
                    () => this.props.fetchProducts()
                }
            >
                <View
                    style = {
                        {
                            padding: 10,
                            margin: 30,
                            backgroundColor: 'purple'
                        }
                    }
                >
                    <Text
                        style = {
                            {
                                color: '#fff',
                                fontSize: 20
                            }
                        }
                    >Add product</Text>
                </View>
            </TouchableOpacity>
        </View>      

    )
  }
}
