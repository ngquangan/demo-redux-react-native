import React, { Component } from 'react'
import {View, Text,SafeAreaView,TouchableOpacity} from 'react-native';
export default class HomeScreen extends Component {

    static navigationOptions = ({navigation}) => {
        
        let header = null;

        return {header}
    }

  render() {
    return (
        <View
            style = {
                {
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }
            }
        >
            <Text>This is HomeScreen</Text>
            <TouchableOpacity
                onPress = {
                    () => this.props.navigation.navigate('Info')
                }
            >
                <View
                    style = {
                        {
                            padding: 10,
                            margin: 30,
                            backgroundColor: 'purple'
                        }
                    }
                >
                    <Text
                        style = {
                            {
                                color: '#fff',
                                fontSize: 20
                            }
                        }
                    >Go to Info Screen</Text>
                </View>
            </TouchableOpacity>
        </View>      

    )
  }
}
